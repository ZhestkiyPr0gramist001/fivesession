package com.example.fivesession.models

data class ModelChatAnswer<T>(
    val type: String,
    val body: T
)
