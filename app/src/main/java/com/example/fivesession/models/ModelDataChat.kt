package com.example.fivesession.models

data class ModelDataChat(
    val chat: ModelItemChat?,
    val message: List<ModelItemMessage>
)

