package com.example.fivesession

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.fivesession.models.ModelDataChat
import com.example.fivesession.models.ModelItemChat
import com.example.fivesession.models.ModelMessage
import com.example.fivesession.models.ModelUser

class ChatActivity : AppCompatActivity(), CallBack {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        Connection.callbacks.add(this)
    }

    override fun onOpen() {

    }

    override fun onMessage(modelMessage: ModelMessage) {

    }

    override fun onChats(chats: List<ModelItemChat>) {

    }

    override fun onChat(chat: ModelDataChat) {

    }

    override fun onPerson(modelUser: ModelUser) {

    }

    override fun onDestroy() {
        super.onDestroy()
        Connection.callbacks.remove(this)
    }


}