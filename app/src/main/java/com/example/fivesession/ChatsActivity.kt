package com.example.fivesession

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.fivesession.models.ModelDataChat
import com.example.fivesession.models.ModelItemChat
import com.example.fivesession.models.ModelMessage
import com.example.fivesession.models.ModelUser

class ChatsActivity : AppCompatActivity(), CallBack {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chats)

        Connection.callbacks.add(this)
        Connection.client.connect()

    }

    override fun onOpen() {
        runOnUiThread{
            Toast.makeText(this, "Open", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onMessage(modelMessage: ModelMessage) {

    }

    override fun onChats(chats: List<ModelItemChat>) {

    }

    override fun onChat(chat: ModelDataChat) {

    }

    override fun onPerson(modelUser: ModelUser) {
        runOnUiThread{
            Toast.makeText(this, "Привет${modelUser.firstname} !", Toast.LENGTH_LONG).show()
        }

    }
}